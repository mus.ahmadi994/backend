package com.example.demo.collaborator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/collaborator")

@CrossOrigin(origins = "*" )

public class CollaboratorController {

    private final CollaboratorService collaboratorService;

    @Autowired
    public CollaboratorController(CollaboratorService collaboratorService) {
        this.collaboratorService =   collaboratorService;
    }

    @GetMapping
    public List<Collaborator> getCollaborators(){

        return collaboratorService.getCollaborators();


    }









    @PostMapping()
    public void registerNewCollaborator(@RequestBody Collaborator collaborator){
    collaboratorService.addNewCollaborator(collaborator);
    }
@DeleteMapping(path = "{collaboratorId}")
public void deleteCollaborator(@PathVariable("collaboratorId") Long collaboratorId ){
    collaboratorService.deleteCollaborator(collaboratorId);
}



    @PutMapping(path = "{collaboratorId}")
    public void updateCollaborator(@RequestBody Collaborator collaborator,@PathVariable("collaboratorId") Long collaboratorId ){
         Collaborator  updateCollaborator =    collaboratorService.getCollaborator(collaboratorId);

         updateCollaborator.setName(collaborator.getName());
        updateCollaborator.setLastName(collaborator.getLastName());

        updateCollaborator.setGender(collaborator.getGender());
        updateCollaborator.setBirthday(collaborator.getBirthday());

         collaboratorService.updateCollaborator(updateCollaborator);





    }

}
